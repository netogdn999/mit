import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';
import 'package:mit/features/mit/domain/repositories/MediaRepository.dart';
import 'package:mit/features/mit/domain/usecases/GetMedias.dart';
import 'package:mockito/mockito.dart';

class MediaRepositorySpy extends Mock implements MediaRepository{}

void main(){
    GetMedias usecase;
    MediaRepositorySpy repositorySpy;
    const medias = const [Media(0, 'test/test'), Media(1, 'test/test')];

    setUp((){
        repositorySpy = MediaRepositorySpy();
        usecase = GetMedias(repositorySpy);
    });

    test(
        'Should get Medias from the repository',
        () async {
            // Given
            when(repositorySpy.getMedias()).thenAnswer((_) async => Right(medias));
            // When
            final result = await usecase();
            // Then
            expect(result, Right(medias));
            verify(repositorySpy.getMedias());
            verifyNoMoreInteractions(repositorySpy);
        }
    );
}