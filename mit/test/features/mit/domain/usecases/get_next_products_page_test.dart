import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mit/features/mit/data/models/ProductModel.dart';
import 'package:mit/features/mit/domain/usecases/get_next_products_page.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
    GetNextProductsPage nextProductsPage;
    final products = (json.decode(fixture('products/products_json.json')) as List<dynamic>).map((json) => ProductModel.fromJson(json)).toList();

    setUp((){
        nextProductsPage = GetNextProductsPage();
    });

    group(
        'GetNextProductsPage',
        () {
            test(
                'Should return a List<Product> contains three itens',
                () {
                    // Given
                    nextProductsPage.products = products;
                    nextProductsPage.productsPerPage = 3;
                    final expected = products.sublist(0, 3);
                    // When
                    final result = nextProductsPage();
                    // Then
                    expect(result, equals(expected));
                }
            );

            test(
                'Should return two pages of Product contains eight and two itens, respectively',
                    () {
                    // Given
                    nextProductsPage.products = products;
                    nextProductsPage.productsPerPage = 8;
                    final expectedPage1 = products.sublist(0, 8);
                    final expectedPage2 = products.sublist(8);
                    // When
                    final resultPage1 = nextProductsPage();
                    final resultPage2 = nextProductsPage();
                    // Then
                    expect(resultPage1, equals(expectedPage1));
                    expect(resultPage2, equals(expectedPage2));
                }
            );

            test(
                'Should return all Products when products per page is bigger then products list\'s size',
                    () {
                    // Given
                    nextProductsPage.products = products;
                    nextProductsPage.productsPerPage = products.length*2;
                    // When
                    final result = nextProductsPage();
                    // Then
                    expect(result, equals(products));
                }
            );
        }
    );
}