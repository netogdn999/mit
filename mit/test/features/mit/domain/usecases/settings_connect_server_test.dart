import 'dart:typed_data';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/features/mit/domain/connection/settings_socket_communication.dart';
import 'package:mit/features/mit/domain/usecases/settings_connect_server.dart';
import 'package:mockito/mockito.dart';

class SettingsSocketCommunicationSpy extends Mock implements SettingsSocketCommunication{}

void main() {
    SettingsSocketCommunicationSpy settingsSocketCommunicationSpy;
    SettingsConnectServer connectServer;

    setUp((){
        settingsSocketCommunicationSpy = SettingsSocketCommunicationSpy();
        connectServer = SettingsConnectServer(settingsSocketCommunicationSpy);
    });

    group(
        'settingsConnectServer',
        () {

            success(Uint8List data) => () {};
            failure(Object error) => (){};
            desconnect() => (){};

            test(
                'Should return true when host and port is a String not empty and a int respectively',
                () async{
                    // Given
                    final host = "127.0.0.1";
                    final port = 4747;
                    when(settingsSocketCommunicationSpy.connect(host, port)).thenAnswer((_) async => Right(true));
                    // When
                    final result = await connectServer.connect(host, port);
                    // Then
                    verify(settingsSocketCommunicationSpy.connect(host, port));
                    expect(result, Right(true));
                    verifyNoMoreInteractions(settingsSocketCommunicationSpy);
                }
            );

            test(
                'Should return SocketFailure when host is empty',
                () async{
                    // Given
                    final host = "";
                    final port = 4747;
                    // When
                    final result = await connectServer.connect(host, port);
                    // Then
                    verifyZeroInteractions(settingsSocketCommunicationSpy);
                    expect(result, Left(SocketFailure()));
                    verifyNoMoreInteractions(settingsSocketCommunicationSpy);
                }
            );

            test(
                'Should call getSettings',
                () async{
                    // When
                    connectServer.getSettings(success, failure, desconnect);
                    // Then
                    verify(settingsSocketCommunicationSpy.getSettings(success, failure, desconnect));
                    verifyNoMoreInteractions(settingsSocketCommunicationSpy);
                }
            );
        }
    );
}