import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';
import 'package:mit/features/mit/domain/repositories/ProductRepository.dart';
import 'package:mit/features/mit/domain/usecases/GetProducts.dart';
import 'package:mockito/mockito.dart';

class ProductRepositorySpy extends Mock implements ProductRepository{}

void main(){
    GetProducts usecase;
    ProductRepositorySpy repositorySpy;
    const products = const [Product(0, "Test", 1.0, false), Product(1, "Test", 2.0, true)];

    setUp((){
        repositorySpy = ProductRepositorySpy();
        usecase = GetProducts(repositorySpy);
    });
    
    test(
        'should get products from the repository',
        () async {
            // Given
            when(repositorySpy.getProducts()).thenAnswer((_) async => Right(products));
            // When
            final result = await usecase();
            // Then
            verify(repositorySpy.getProducts());
            expect(result, Right(products));
            verifyNoMoreInteractions(repositorySpy);
        }
    );
}