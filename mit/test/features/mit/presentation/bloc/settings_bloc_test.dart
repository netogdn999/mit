import 'dart:convert';
import 'dart:typed_data';

import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/core/util/input_converter.dart';
import 'package:mit/features/mit/data/models/SettingsModel.dart';
import 'package:mit/features/mit/domain/usecases/settings_connect_server.dart';
import 'package:mit/features/mit/presentation/bloc/settings_bloc.dart';
import 'package:mockito/mockito.dart';

class SettingsConnectServerSpy extends Mock implements SettingsConnectServer{} // ignore: must_be_immutable

void main() {
    SettingsConnectServer connectServer;
    InputConverter converter;
    SettingsBloc bloc;
    final host = "127.0.0.1";
    final port = 4747;

    setUp(() {
        connectServer = SettingsConnectServerSpy();
        converter = InputConverter();
        bloc = SettingsBloc(connectServer: connectServer, converter: converter);
    });

    tearDown((){
        bloc.close();
    });

    test(
        'State should be SettingsInitial',
            () {
            // Then
            expect(bloc.state, isA<SettingsInitial>());
        }
    );

    group(
        'ConnectToServer',
        () {
            test(
                'Should connect to server',
                () async {
                    // When
                    bloc.add(ConnectToServer(host, port));
                    await untilCalled(connectServer.connect(host, port));
                    // Then
                    verify(connectServer.connect(host, port));
                }
            );

            test(
                'Should call getSettings when connect gotten successfully',
                () async {
                    // Given
                    when(connectServer.connect(host, port)).thenAnswer((_) async => Right(true));
                    // When
                    bloc.add(ConnectToServer(host, port));
                    await untilCalled(connectServer.getSettings(bloc.socketSuccessMessage, bloc.socketFailureMessage, bloc.socketDisconnect));
                    // Then
                    verify(connectServer.connect(host, port));
                    verify(connectServer.getSettings(bloc.socketSuccessMessage, bloc.socketFailureMessage, bloc.socketDisconnect));
                    verifyNoMoreInteractions(connectServer);
                }
            );

            blocTest(
                'Should emit [SettingsEstablishingCommunicationToServer, SettingsEstablishedCommunicationSuccess] when connect is gotten successfully',
                build: () {
                    when(connectServer.connect(host, port)).thenAnswer((_) async => Right(true));
                    return bloc;
                },
                act: (bloc) => bloc.add(ConnectToServer(host, port)),
                expect: [SettingsInitial(), SettingsEstablishingCommunicationToServer(), SettingsEstablishedCommunicationSuccess()]
            );

            blocTest(
                'Should emit [SettingsEstablishingCommunicationToServer, SettingsEstablishedCommunicationError] when connect is getting fail',
                build: () {
                    when(connectServer.connect(host, port)).thenAnswer((_) async => Left(SocketFailure()));
                    return bloc;
                },
                act: (bloc) => bloc.add(ConnectToServer(host, port)),
                expect: [SettingsInitial(), SettingsEstablishingCommunicationToServer(), SettingsEstablishedCommunicationError(SOCKET_FAILURE_MESSAGE)]
            );
        }
    );

    group(
        'UpdatedMedias',
        () {
            final durationMediaTransition = 1000;
            blocTest(
                'Should emit [SettingsUpdateMedias]',
                build: () => bloc,
                act: (bloc) => bloc.add(UpdatedMedias(durationMediaTransition)),
                expect: [SettingsInitial(), SettingsUpdateMedias(durationMediaTransition)]
            );
        }
    );

    group(
        'UpdatedProducts',
            () {
            final durationProductPageTransition = 1000;
            final quantityProductsPerPage = 10;
            blocTest(
                'Should emit [SettingsUpdateProducts]',
                build: () => bloc,
                act: (bloc) => bloc.add(UpdatedProducts(durationProductPageTransition, quantityProductsPerPage)),
                expect: [SettingsInitial(), SettingsUpdateProducts(durationProductPageTransition, quantityProductsPerPage)]
            );
        }
    );

    group(
        'ConnectionError',
            () {
            blocTest(
                'Should emit [SettingsEstablishedCommunicationError]',
                build: () => bloc,
                act: (bloc) => bloc.add(ConnectionError(SOCKET_FAILURE_MESSAGE)),
                expect: [SettingsInitial(), SettingsEstablishedCommunicationError(SOCKET_FAILURE_MESSAGE)]
            );
        }
    );

    group(
        'socketSuccessMessage',
        () {
            final settingsUpdateMedias = SettingsModel(0, 1000, 2000, 3000, false, true);
            final settingsUpdateProducts = SettingsModel(0, 1000, 2000, 3000, true, false);
            final settingsUpdateMediasProducts = SettingsModel(0, 1000, 2000, 3000, true, true);

            final settingsUpdateMediasUint8List = Uint8List.fromList(json.encode(settingsUpdateMedias.toJson()).codeUnits);
            final settingsUpdateProductsUint8List = Uint8List.fromList(json.encode(settingsUpdateProducts.toJson()).codeUnits);
            final settingsUpdateMediasProductsUint8List = Uint8List.fromList(json.encode(settingsUpdateMediasProducts.toJson()).codeUnits);

            blocTest(
                'Should emit [SettingsUpdateMedias] when isUpdatedMedias\'s Settings  is true',
                build: () => bloc,
                act: (bloc) => bloc.socketSuccessMessage(settingsUpdateMediasUint8List),
                expect: [SettingsInitial(), SettingsUpdateMedias(settingsUpdateMedias.durationMediaTransition)]
            );

            blocTest(
                'Should emit [SettingsUpdateProducts] when isUpdatedProducts\'s Settings  is true',
                build: () => bloc,
                act: (bloc) => bloc.socketSuccessMessage(settingsUpdateProductsUint8List),
                expect: [SettingsInitial(), SettingsUpdateProducts(settingsUpdateProducts.durationProductPageTransition, settingsUpdateProducts.quantityProductsPerPage)]
            );

            blocTest(
                'Should emit [SettingsUpdateMedias, SettingsUpdateProducts] when isUpdatedMedias and isUpdatedProducts\'s Settings are true',
                build: () => bloc,
                act: (bloc) => bloc.socketSuccessMessage(settingsUpdateMediasProductsUint8List),
                expect: [SettingsInitial(), SettingsUpdateMedias(settingsUpdateMedias.durationMediaTransition), SettingsUpdateProducts(settingsUpdateProducts.durationProductPageTransition, settingsUpdateProducts.quantityProductsPerPage)]
            );
        }
    );

    group(
        'socketFailureMessage',
            () {
            blocTest(
                'Should emit [SettingsEstablishedCommunicationError] when socketFailureMessage event is called',
                build: () => bloc,
                act: (bloc) => bloc.socketFailureMessage(null),
                expect: [SettingsInitial(), SettingsEstablishedCommunicationError(SOCKET_FAILURE_MESSAGE)]
            );
        }
    );

    group(
        'socketDesconnect',
            () {
            blocTest(
                'Should emit [SettingsEstablishedCommunicationError] when socketDesconnect is called',
                build: () => bloc,
                act: (bloc) => bloc.socketDisconnect(),
                expect: [SettingsInitial(), SettingsEstablishedCommunicationError(SOCKET_FAILURE_MESSAGE)]
            );
        }
    );
}