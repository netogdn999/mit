import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';
import 'package:mit/features/mit/domain/usecases/GetMedias.dart';
import 'package:mit/features/mit/presentation/bloc/media_bloc.dart';
import 'package:mockito/mockito.dart';

class GetMediasSpy extends Mock implements GetMedias {} // ignore: must_be_immutable

void main() {
    MediaBloc bloc;
    GetMediasSpy getMedias;

    setUp((){
        getMedias = GetMediasSpy();
        bloc = MediaBloc(getMedias: getMedias);
    });

    tearDown((){
        bloc.close();
    });

    test(
        'State should be MediaInitial',
        (){
            // Then
            expect(bloc.state, isA<MediaInitial>());
        }
    );

    group(
        'GetMedias',
        () {
            const medias = const [Media(0, 'test/test'), Media(1, 'test/test')];

            test(
                'Should get medias',
                () async{
                    // Given
                    when(getMedias()).thenAnswer((_) async => Right(medias));
                    // When
                    bloc.add(GetNextMediasPage());
                    await untilCalled(getMedias());
                    // Then
                    verify(getMedias());
                }
            );

            blocTest(
                'Should emit [Loading, Loaded] when data is gotten successfully',
                build: () {
                    when(getMedias()).thenAnswer((_) async => Right(medias));
                    return bloc;
                },
                act: (bloc) => bloc.add(GetNextMediasPage()),
                expect: [MediaInitial(), MediaLoading(), MediaLoaded(medias)]
            );

            blocTest(
                'Should emit [Loading, Error] when data is getting data fails',
                build: () {
                    when(getMedias()).thenAnswer((_) async => Left(ServerFailure()));
                    return bloc;
                },
                act: (bloc) => bloc.add(GetNextMediasPage()),
                expect: [MediaInitial(), MediaLoading(), MediaError(SERVER_FAILURE_MESSAGE)]
            );

            blocTest(
                'Should emit [Loading, Error] with a proper message for the error when getting data fails',
                build: () {
                    when(getMedias()).thenAnswer((_) async => Left(CacheFailure()));
                    return bloc;
                },
                act: (bloc) => bloc.add(GetNextMediasPage()),
                expect: [MediaInitial(), MediaLoading(), MediaError(CACHE_FAILURE_MESSAGE)]
            );
        }
    );
}