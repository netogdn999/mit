import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';
import 'package:mit/features/mit/domain/usecases/GetProducts.dart';
import 'package:mit/features/mit/domain/usecases/get_next_products_page.dart';
import 'package:mit/features/mit/presentation/bloc/product_bloc.dart';
import 'package:mockito/mockito.dart';

class GetProductsSpy extends Mock implements GetProducts {} // ignore: must_be_immutable
class GetNextProductsPageSpy extends Mock implements GetNextProductsPage {} // ignore: must_be_immutable

void main() {
    ProductBloc bloc;
    GetProductsSpy getProducts;
    GetNextProductsPageSpy nextProductsPageSpy;

    setUp((){
        getProducts = GetProductsSpy();
        nextProductsPageSpy = GetNextProductsPageSpy();
        bloc = ProductBloc(getProducts, nextProductsPageSpy);
    });

    tearDown((){
        bloc.close();
    });

    test(
        'State should be ProductInitial',
        (){
            // Then
            expect(bloc.state, isA<ProductInitial>());
        }
    );

    group(
        'GetProducts',
        () {
            const products = const [Product(0, 'Test', 1.0, false), Product(1, 'Test', 2.0, true)];

            test(
                'Should get products',
                () async{
                    // Given
                    when(getProducts()).thenAnswer((_) async => Right(products));
                    // When
                    bloc.add(GetProductsEvent());
                    await untilCalled(getProducts());
                    // Then
                    verify(getProducts());
                }
            );

            blocTest(
                'Should emit [Loading, Loaded] when data is gotten successfully',
                build: () {
                    when(getProducts()).thenAnswer((_) async => Right(products));
                    when(bloc.nextProductsPage()).thenAnswer((_) => products);
                    return bloc;
                },
                act: (bloc) => bloc.add(GetProductsEvent()),
                expect: [ProductInitial(), ProductLoading(), ProductLoaded(products)]
            );

            blocTest(
                'Should emit [Loading, Error] when data is getting data fails',
                build: () {
                    when(getProducts()).thenAnswer((_) async => Left(ServerFailure()));
                    return bloc;
                },
                act: (bloc) => bloc.add(GetProductsEvent()),
                expect: [ProductInitial(), ProductLoading(), ProductError(SERVER_FAILURE_MESSAGE)]
            );

            blocTest(
                'Should emit [Loading, Error] with a proper message for the error when getting data fails',
                build: () {
                    when(getProducts()).thenAnswer((_) async => Left(CacheFailure()));
                    return bloc;
                },
                act: (bloc) => bloc.add(GetProductsEvent()),
                expect: [ProductInitial(), ProductLoading(), ProductError(CACHE_FAILURE_MESSAGE)]
            );
        }
    );

    group(
        'UpdateProductsValuesEvent',
        () {
            const products = const [Product(0, 'Test', 1.0, false), Product(1, 'Test', 2.0, true)];
            final productsPerPage = 10;
            final durationTransitionPage = 2;

            blocTest(
                'Should emit [Loading, Loaded] when UpdateProductsValuesEvent is added',
                build: () {
                    when(getProducts()).thenAnswer((_) async => Right(products));
                    when(bloc.nextProductsPage()).thenAnswer((_) => products);
                    return bloc;
                },
                act: (bloc) => bloc.add(UpdateProductsValuesEvent(productsPerPage, durationTransitionPage)),
                expect: [ProductInitial(), ProductLoading(), ProductLoaded(products)]
            );
        }
    );

    group(
        'GetNextProductsPageEvent',
            () {
            const products = const [Product(0, 'Test', 1.0, false), Product(1, 'Test', 2.0, true)];

            blocTest(
                'Should emit [Loaded] when GetNextProductsPageEvent is added',
                build: () {
                    when(bloc.nextProductsPage()).thenAnswer((_) => products);
                    return bloc;
                },
                act: (bloc) => bloc.add(GetNextProductsPageEvent()),
                expect: [ProductInitial(), ProductLoopPage(products)]
            );
        }
    );
}