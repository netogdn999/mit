import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/features/mit/data/datasources/MediaLocalDataSource.dart';
import 'package:mit/features/mit/data/models/MediaModel.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:matcher/matcher.dart';

import '../../../../fixtures/fixture_reader.dart';

class SharePreferencesImpSpy extends Mock implements SharedPreferences {}

void main() {
    MediaLocalDataSourceImp dataSource;
    SharePreferencesImpSpy sharePreferencesImpSpy;

    setUp(() {
        sharePreferencesImpSpy = SharePreferencesImpSpy();
        dataSource = MediaLocalDataSourceImp(sharedPreferences: sharePreferencesImpSpy);
    });

    group(
        'getMediasCached',
        () {
            final mediasModel = (json.decode(fixture('medias/medias_cached.json')) as List<dynamic>).map((json) => MediaModel.fromJson(json)).toList();

            test(
                'Should return Medias from SharedPreferences when there is medias in the cache',
                () async{
                    // Given
                    when(sharePreferencesImpSpy.getString(any)).thenAnswer((realInvocation) => fixture('medias/medias_cached.json'));
                    // When
                    final result = await dataSource.getMedias();
                    // Then
                    verify(sharePreferencesImpSpy.getString(any));
                    expect(result, mediasModel);
                }
            );

            test(
                'Should throw a CacheException when there is not a cached value',
                () {
                    // Given
                    when(sharePreferencesImpSpy.getString(any)).thenReturn(null);
                    // When
                    final call = dataSource.getMedias;
                    // Then
                    expect(() => call(), throwsA(isA<CacheException>()));
                }
            );
        }
    );

    group(
        'CacheMedias',
        () {
            const mediasModel = const [MediaModel(0, "test/test"), MediaModel(1, "test/test")];

            test(
                'Should call SharedPreferences to cache the data',
                () {
                    // Given
                    final expectedJsonString = json.encode(mediasModel.map((media) => media.toJson()).toList());
                    // When
                    dataSource.putMedias(mediasModel);
                    //Then
                    verify(sharePreferencesImpSpy.setString(CACHED_MEDIAS, expectedJsonString));
                }
            );
        }
    );
}