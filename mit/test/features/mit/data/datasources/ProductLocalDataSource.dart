import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/features/mit/data/datasources/ProductLocalDataSource.dart';
import 'package:mit/features/mit/data/models/ProductModel.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:matcher/matcher.dart';

import '../../../../fixtures/fixture_reader.dart';

class SharePreferencesImpSpy extends Mock implements SharedPreferences {}

void main() {
    ProductLocalDataSourceImp dataSource;
    SharePreferencesImpSpy sharedPreferencesImpSpy;

    setUp(() {
        sharedPreferencesImpSpy = SharePreferencesImpSpy();
        dataSource = ProductLocalDataSourceImp(sharedPreferences: sharedPreferencesImpSpy);
    });

    group(
        'getProductsCached',
        () {
            final productsModel = (json.decode(fixture('products/products_cached.json')) as List<dynamic>).map((json) => ProductModel.fromJson(json)).toList();

            test(
                'Should return Products from SharedPreferences when there is products in the cache',
                () async{
                    // Given
                    when(sharedPreferencesImpSpy.getString(any)).thenAnswer((realInvocation) => fixture('products/products_cached.json'));
                    // When
                    final result = await dataSource.getProducts();
                    // Then
                    verify(sharedPreferencesImpSpy.getString(any));
                    expect(result, productsModel);
                }
            );

            test(
                'Should throw a CacheException when there is not a cached value',
                () {
                    // Given
                    when(sharedPreferencesImpSpy.getString(any)).thenReturn(null);
                    // When
                    final call = dataSource.getProducts;
                    // Then
                    expect(() => call(), throwsA(isA<CacheException>()));
                }
            );
        }
    );

    group(
        'CacheProducts',
        () {
            const productsModel = const [ProductModel(0, "Test", 1.0, false), ProductModel(1, "Test", 2.0, true)];

            test(
                'Should call SharedPreferences to cache the data',
                () {
                    // Given
                    final expectedJsonString = json.encode(productsModel.map((product) => product.toJson()).toList());
                    // When
                    dataSource.putProducts(productsModel);
                    //Then
                    verify(sharedPreferencesImpSpy.setString(CACHED_PRODUCTS, expectedJsonString));
                }
            );
        }
    );
}