import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/features/mit/data/datasources/MediaRemoteDataSource.dart';
import 'package:mit/features/mit/data/models/MediaModel.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_reader.dart';

class HttpClienteSpy extends Mock implements http.Client {}

void main() {
    MediaRemoteDataSourceImp dataSourceImp;
    HttpClienteSpy httpClienteSpy;

    setUp(() {
        httpClienteSpy = HttpClienteSpy();
        dataSourceImp = MediaRemoteDataSourceImp(client: httpClienteSpy);
    });

    void setUpMockHttpClientSuccess200(){
        when(httpClienteSpy.get(any, headers: anyNamed('headers'))).thenAnswer((_) async => http.Response(fixture('medias/medias_json.json'), 200));
    }

    void setUpMockHttpClientFailure404(){
        when(httpClienteSpy.get(any, headers: anyNamed('headers'))).thenAnswer((_) async => http.Response('Something went wrong', 404));
    }

    group(
        'getMedias',
        () {
            final mediasModel = (json.decode(fixture('medias/medias_json.json')) as List<dynamic>).map((json) => MediaModel.fromJson(json)).toList();

            test(
                'Should preform a GET request on a URL the endpoint and with application/json header',
                () {
                    // Given
                    setUpMockHttpClientSuccess200();
                    // When
                    dataSourceImp.getMedias();
                    // Then
                    verify(httpClienteSpy.get('api url', headers: {'Content-Type': 'application/json'}));
                }
            );

            test(
                'Should return Medias when the response code is 200 (success)',
                () async{
                    // Given
                    setUpMockHttpClientSuccess200();
                    // When
                    final result = await dataSourceImp.getMedias();
                    // Then
                    expect(result, mediasModel);
                }
            );

            test(
                'Should throw a ServerException when the response code is 404 or other',
                () {
                    // Given
                    setUpMockHttpClientFailure404();
                    // When
                    final call = dataSourceImp.getMedias;
                    // Then
                    expect(() => call(), throwsA(isA<ServerException>()));
                }
            );
        }
    );
}