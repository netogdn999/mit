import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/features/mit/data/datasources/ProductRemoteDataSource.dart';
import 'package:mit/features/mit/data/models/ProductModel.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_reader.dart';

class HttpClienteSpy extends Mock implements http.Client {}

void main() {
    ProductRemoteDataSourceImp dataSourceImp;
    HttpClienteSpy httpClienteSpy;

    setUp(() {
        httpClienteSpy = HttpClienteSpy();
        dataSourceImp = ProductRemoteDataSourceImp(client: httpClienteSpy);
    });

    void setUpMockHttpClientSuccess200(){
        when(httpClienteSpy.get(any, headers: anyNamed('headers'))).thenAnswer((_) async => http.Response(fixture('products/products_json.json'), 200));
    }

    void setUpMockHttpClientFailure404(){
        when(httpClienteSpy.get(any, headers: anyNamed('headers'))).thenAnswer((_) async => http.Response('Something went wrong', 404));
    }

    group(
        'getProducts',
        () {
            final productsModel = (json.decode(fixture('products/products_json.json')) as List<dynamic>).map((json) => ProductModel.fromJson(json)).toList();

            test(
                'Should preform a GET request on a URL the endpoint and with application/json header',
                () {
                    // Given
                    setUpMockHttpClientSuccess200();
                    // When
                    dataSourceImp.getProducts();
                    // Then
                    verify(httpClienteSpy.get('api url', headers: {'Content-Type': 'application/json'}));
                }
            );

            test(
                'Should return Products when the response code is 200 (success)',
                () async{
                    // Given
                    setUpMockHttpClientSuccess200();
                    // When
                    final result = await dataSourceImp.getProducts();
                    // Then
                    expect(result, productsModel);
                }
            );

            test(
                'Should throw a ServerException when the response code is 404 or other',
                () {
                    // Given
                    setUpMockHttpClientFailure404();
                    // When
                    final call = dataSourceImp.getProducts;
                    // Then
                    expect(() => call(), throwsA(isA<ServerException>()));
                }
            );
        }
    );
}