import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/core/network/NetworkInfo.dart';
import 'package:mit/features/mit/data/datasources/ProductLocalDataSource.dart';
import 'package:mit/features/mit/data/datasources/ProductRemoteDataSource.dart';
import 'package:mit/features/mit/data/models/ProductModel.dart';
import 'package:mit/features/mit/data/repositories/ProductRepositoryImp.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';
import 'package:mockito/mockito.dart';

class ProductRemoteDataSourceSpy extends Mock implements ProductRemoteDataSource {}

class ProductLocalDataSourceSpy extends Mock implements ProductLocalDataSource {}

class NetworkInfoSpy extends Mock implements NetworkInfo {}

void main() {
    ProductRepositoryImp repository;
    ProductRemoteDataSourceSpy remoteDataSourceSpy;
    ProductLocalDataSourceSpy localDataSourceSpy;
    NetworkInfoSpy networkInfoSpy;

    setUp((){
        remoteDataSourceSpy = ProductRemoteDataSourceSpy();
        localDataSourceSpy = ProductLocalDataSourceSpy();
        networkInfoSpy = NetworkInfoSpy();
        repository = ProductRepositoryImp(
            remoteDataSource: remoteDataSourceSpy,
            localDataSource: localDataSourceSpy,
            networkInfo: networkInfoSpy
        );
    });

    void runTestsOnline(Function body) {
        group(
            'device is online',
            () {
                setUp(() {
                    when(networkInfoSpy.isConnect).thenAnswer((_) async => true);
                });

                body();
            }
        );
    }

    void runTestsOffline(Function body) {
        group(
            'device is offline',
            () {
                setUp(() {
                    when(networkInfoSpy.isConnect).thenAnswer((_) async => false);
                });

                body();
            }
        );
    }

    group(
        'getProducts',
            () {
            const productsModel = const [ProductModel(0, "Test", 1.0, false), ProductModel(1, "Test", 2.0, true)];
            const List<Product> products = productsModel;

            test(
                'should check if device is online',
                () {
                    // Given
                    when(networkInfoSpy.isConnect).thenAnswer((_) async => true);
                    // When
                    repository.getProducts();
                    // Then
                    verify(networkInfoSpy.isConnect);
                }
            );

            runTestsOnline(() {
                test(
                    'Should return remote data when the call to remote data source is successful',
                    () async {
                        // Given
                        when(remoteDataSourceSpy.getProducts()).thenAnswer((_) async => const [ProductModel(0, "Test", 1.0, false), ProductModel(1, "Test", 2.0, true)]);
                        // When
                        final result = await repository.getProducts();
                        // Then
                        verify(remoteDataSourceSpy.getProducts());
                        expect(result, Right(products));
                    }
                );

                test(
                    'Should cache the data locally when the call to remote data source is successful',
                    () async{
                        // Given
                        when(remoteDataSourceSpy.getProducts()).thenAnswer((_) async => products);
                        // When
                        await repository.getProducts();
                        // Then
                        verify(remoteDataSourceSpy.getProducts());
                        verify(localDataSourceSpy.putProducts(products));
                    }
                );

                test(
                    'Should return ServerFailure when the call to remote data source is unsuccessful',
                    () async{
                        // Give
                        when(remoteDataSourceSpy.getProducts()).thenThrow(ServerException());
                        // When
                        final result = await repository.getProducts();
                        // Then
                        verify(remoteDataSourceSpy.getProducts());
                        verifyZeroInteractions(localDataSourceSpy);
                        expect(result, Left(ServerFailure()));
                    }
                );
            });

            runTestsOffline(() {
                test(
                    'should return last locally cached data when the cached data is present',
                    () async{
                        // Given
                        when(localDataSourceSpy.getProducts()).thenAnswer((_) async => const [ProductModel(0, "Test", 1.0, false), ProductModel(1, "Test", 2.0, true)]);
                        // When
                        final result = await repository.getProducts();
                        // Then
                        verifyZeroInteractions(remoteDataSourceSpy);
                        verify(localDataSourceSpy.getProducts());
                        expect(result, Right(products));
                    }
                );

                test(
                    'should return CacheFailure when there is no cached data present',
                    () async{
                        // Given
                        when(localDataSourceSpy.getProducts()).thenThrow(CacheException());
                        // When
                        final result = await repository.getProducts();
                        // Then
                        verifyZeroInteractions(remoteDataSourceSpy);
                        verify(localDataSourceSpy.getProducts());
                        expect(result, Left(CacheFailure()));
                    }
                );
            });
        }
    );
}