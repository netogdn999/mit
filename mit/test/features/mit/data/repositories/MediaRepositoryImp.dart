import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/core/network/NetworkInfo.dart';
import 'package:mit/features/mit/data/datasources/MediaLocalDataSource.dart';
import 'package:mit/features/mit/data/datasources/MediaRemoteDataSource.dart';
import 'package:mit/features/mit/data/models/MediaModel.dart';
import 'package:mit/features/mit/data/repositories/MediaRepositoryImp.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';
import 'package:mockito/mockito.dart';

class MediaRemoteDataSourceSpy extends Mock implements MediaRemoteDataSource {}

class MediaLocalDataSourceSpy extends Mock implements MediaLocalDataSource {}

class NetworkInfoSpy extends Mock implements NetworkInfo {}

void main() {
    MediaRepositoryImp repository;
    MediaRemoteDataSourceSpy remoteDataSourceSpy;
    MediaLocalDataSourceSpy localDataSourceSpy;
    NetworkInfoSpy networkInfoSpy;

    setUp((){
        remoteDataSourceSpy = MediaRemoteDataSourceSpy();
        localDataSourceSpy = MediaLocalDataSourceSpy();
        networkInfoSpy = NetworkInfoSpy();
        repository = MediaRepositoryImp(
            remoteDataSource: remoteDataSourceSpy,
            localDataSource: localDataSourceSpy,
            networkInfo: networkInfoSpy
        );
    });

    void runTestsOnline(Function body) {
        group(
            'device is online',
            () {
                setUp(() {
                    when(networkInfoSpy.isConnect).thenAnswer((_) async => true);
                });

                body();
            }
        );
    }

    void runTestsOffline(Function body) {
        group(
            'device is offline',
            () {
                setUp(() {
                    when(networkInfoSpy.isConnect).thenAnswer((_) async => false);
                });

                body();
            }
        );
    }

    group(
        'getMedias',
        () {
           const mediasModel = const [MediaModel(0, "test/test"), MediaModel(1, "test/test")];
           const List<Media> medias = mediasModel;

           test(
               'should check if device is online',
               () {
                   // Given
                   when(networkInfoSpy.isConnect).thenAnswer((_) async => true);
                   // When
                   repository.getMedias();
                   // Then
                   verify(networkInfoSpy.isConnect);
               }
           );

           runTestsOnline(() {
               test(
                   'Should return remote data when the call to remote data source is successful',
                   () async {
                       // Given
                       when(remoteDataSourceSpy.getMedias()).thenAnswer((_) async => const [MediaModel(0, "test/test"), MediaModel(1, "test/test")]);
                       // When
                       final result = await repository.getMedias();
                       // Then
                       verify(remoteDataSourceSpy.getMedias());
                       expect(result, equals(Right(medias)));
                   }
               );

               test(
                   'Should cache the data locally when the call to remote data source is successful',
                   () async{
                       // When
                       await repository.getMedias();
                       // Then
                       verify(remoteDataSourceSpy.getMedias());
                       verify(localDataSourceSpy.putMedias(any));
                   }
               );

               test(
                   'Should return ServerFailure when the call to remote data source is unsuccessful',
                   () async{
                       // Given
                       when(remoteDataSourceSpy.getMedias()).thenThrow(ServerException());
                       // When
                       final result = await repository.getMedias();
                       // Then
                       verify(remoteDataSourceSpy.getMedias());
                       verifyZeroInteractions(localDataSourceSpy);
                       expect(result, Left(ServerFailure()));
                   }
               );
           });

           runTestsOffline(() {
               test(
                   'should return last locally cached data when the cached data is present',
                   () async{
                       // Given
                       when(localDataSourceSpy.getMedias()).thenAnswer((_) async => const [MediaModel(0, "test/test"), MediaModel(1, "test/test")]);
                       // When
                       final result = await repository.getMedias();
                       // Then
                       verifyZeroInteractions(remoteDataSourceSpy);
                       verify(localDataSourceSpy.getMedias());
                       expect(result, Right(medias));
                   }
               );

               test(
                   'should return CacheFailure when there is no cached data present',
                   () async{
                       // Given
                       when(localDataSourceSpy.getMedias()).thenThrow(CacheException());
                       // When
                       final result = await repository.getMedias();
                       // Then
                       verifyZeroInteractions(remoteDataSourceSpy);
                       verify(localDataSourceSpy.getMedias());
                       expect(result, Left(CacheFailure()));
                   }
               );
           });
        }
    );
}