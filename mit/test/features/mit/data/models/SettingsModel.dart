import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mit/features/mit/data/models/SettingsModel.dart';
import 'package:mit/features/mit/domain/entities/Settings.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
    final List<SettingsModel> models = [SettingsModel(0, 1000, 2000, 3000, false, false), SettingsModel(1, 4000, 5000, 6000, true, true)];

    test(
        'Should be a subclass of Settings entity',
        () {
            // Then
            expect(models, isA<List<Settings>>());
        }
    );
    
    group(
        'fromJson',
        () {
            test(
                'Should return a valid model when the JSON all field is integer',
                () {
                    // Given
                    final List<dynamic> jsonMap = json.decode(fixture('settings/settings_json.json'));
                    // When
                    final result = jsonMap.map((json) => SettingsModel.fromJson(json)).toList();
                    // Then
                    expect(result, models);
                }
            );
        }
    );

    group(
        'toJson',
        () {
            test(
                'Should return a JSON map containing te proper data',
                    () async {
                    // When
                    final result = models.map((model) => model.toJson()).toList();
                    final expectJson = json.decode(fixture('settings/settings_json.json'));
                    // Then
                    expect(result, expectJson);
                }
            );
        }
    );
}