import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mit/features/mit/data/models/ProductModel.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
    const List<ProductModel> models = const [ProductModel(0, 'Test', 1.0, false), ProductModel(1, 'Test', 2.0, true)];

    test(
        'Should be a subclass of Product entity',
        () {
            // Then
            expect(models, isA<List<Product>>());
        }
    );
    
    group(
        'fromJson',
        () {
            test(
                'Should return a valid model when the JSON field is integer, string, double and bool respectively',
                () {
                    // Given
                    final List<dynamic> jsonMap = json.decode(fixture('products/products_json.json'));
                    // When
                    final result = jsonMap.map((json) => ProductModel.fromJson(json)).toList();
                    // Then
                    expect(result, models);
                }
            );
        }
    );

    group(
        'toJson',
        () {
            test(
                'Should return a JSON map containing te proper data',
                    () async {
                    // When
                    final result = models.map((model) => model.toJson()).toList();
                    final expectJson = json.decode(fixture('products/products_json.json'));
                    // Then
                    expect(result, expectJson);
                }
            );
        }
    );
}