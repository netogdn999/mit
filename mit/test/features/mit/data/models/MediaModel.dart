import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mit/features/mit/data/models/MediaModel.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
    final List<MediaModel> models = [MediaModel(0, 'test/test'), MediaModel(1, 'test/test')];

    test(
        'Should be a subclass of Media entity',
        () {
            // Then
            expect(models, isA<List<Media>>());
        }
    );
    
    group(
        'fromJson',
        () {
            test(
                'Should return a valid model when the JSON field is integer and string respectively',
                () {
                    // Given
                    final List<dynamic> jsonMap = json.decode(fixture('medias/medias_json.json'));
                    // When
                    final result = jsonMap.map((json) => MediaModel.fromJson(json)).toList();
                    // Then
                    expect(result, models);
                }
            );
        }
    );

    group(
        'toJson',
        () {
            test(
                'Should return a JSON map containing te proper data',
                () async {
                    // When
                    final result = models.map((model) => model.toJson()).toList();
                    final expectJson = json.decode(fixture('medias/medias_json.json'));
                    // Then
                    expect(result, expectJson);
                }
            );
        }
    );
}