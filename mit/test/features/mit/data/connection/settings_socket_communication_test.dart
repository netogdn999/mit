import 'dart:typed_data';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/core/network/socket_communication.dart';
import 'package:mit/features/mit/data/connection/settings_socket_communication_imp.dart';
import 'package:mockito/mockito.dart';

class SocketCommunicationSpy extends Mock implements SocketCommunication {}

void main() {
    SocketCommunicationSpy socketCommunicationSpy;
    SettingsSocketCommunicationImp settingsSocketCommunicationImp;

    setUp((){
        socketCommunicationSpy = SocketCommunicationSpy();
        settingsSocketCommunicationImp = SettingsSocketCommunicationImp(socketCommunicationSpy);
    });

    group(
        'settingsSocketCommunication',
        (){
            final host = "";
            final port = 4747;

            success(Uint8List data) => () {};
            failure(Object error) => (){};
            desconnect() => (){};

            test(
                'Should connect gotten successfully',
                () {
                    // Given
                    when(socketCommunicationSpy.connect(host, port)).thenAnswer((_) => null);
                    // When
                    settingsSocketCommunicationImp.connect(host, port);
                    // Then
                    verify(socketCommunicationSpy.connect(host, port));
                }
            );

            test(
                'Should return true when connect gotten successfully',
                () async{
                    // Given
                    when(socketCommunicationSpy.connect(host, port)).thenAnswer((_) => null);
                    // When
                    final result = await settingsSocketCommunicationImp.connect(host, port);
                    // Then
                    verify(socketCommunicationSpy.connect(host, port));
                    expect(result, Right(true));
                }
            );

            test(
                'Should return SocketFailure when connect getting fail',
                () async{
                    // Given
                    when(socketCommunicationSpy.connect(host, port)).thenThrow(SettingSocketException());
                    // When
                    final result = await settingsSocketCommunicationImp.connect(host, port);
                    // Then
                    verify(socketCommunicationSpy.connect(host, port));
                    expect(result, Left(SocketFailure()));
                }
            );

            test(
                'Should call listen when getSettings was called',
                () {
                    // when
                    settingsSocketCommunicationImp.getSettings(success, failure, desconnect);
                    // Then
                    verify(socketCommunicationSpy.getSettings(success, failure, desconnect));
                }
            );
        }
    );
}