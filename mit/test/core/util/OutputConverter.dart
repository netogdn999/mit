import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/util/OutputConverter.dart';

void main(){
    OutputConverter converter;

    setUp((){
        converter = OutputConverter();
    });

    group(
        'numberToMoney',
        () {
            test(
                'Should return an money formated value when number represents a value valid for money',
                () {
                    // Given
                    final value = 1000.4;
                    final expected = "R\$ 1.000,40/Kg";
                    // When
                    final result = converter.numberToMoneyPerKilo(value);
                    // Then
                    expect(result, expected);
                }
            );
        }
    );
}