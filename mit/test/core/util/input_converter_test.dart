import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/util/input_converter.dart';

void main(){
    InputConverter converter;

    setUp((){
        converter = InputConverter();
    });

    group(
        'uint8List to String',
        () {
            test(
                'Should return a String formated value when Uint8List represents a String',
                () {
                    // Given
                    final Uint8List value = Uint8List.fromList([65]);
                    final expected = "A";
                    // When
                    final result = converter.uint8ListToString(value);
                    // Then
                    expect(result, expected);
                }
            );
        }
    );
}