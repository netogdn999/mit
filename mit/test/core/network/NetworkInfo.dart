import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mit/core/network/NetworkInfo.dart';
import 'package:mockito/mockito.dart';

class DataConnectionCheckerSpy extends Mock implements DataConnectionChecker {}

void main() {
    NetworkInfoImp networkInfo;
    DataConnectionCheckerSpy dataConnectionCheckerSpy;

    setUp(() {
        dataConnectionCheckerSpy = DataConnectionCheckerSpy();
        networkInfo = NetworkInfoImp(dataConnectionCheckerSpy);
    });

    group(
        'isConnected',
        () {
            test(
                'should forward the call to DataConnectionChecker.hasConnection',
                () {
                    // Given
                    final hasConnectionFuture = Future.value(true);
                    when(dataConnectionCheckerSpy.hasConnection).thenAnswer((_) => hasConnectionFuture);
                    // When
                    final result = networkInfo.isConnect;
                    // Then
                    verify(dataConnectionCheckerSpy.hasConnection);
                    expect(result, hasConnectionFuture);
                }
            );
        }
    );
}