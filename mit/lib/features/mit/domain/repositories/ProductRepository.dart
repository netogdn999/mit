import 'package:dartz/dartz.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';

abstract class ProductRepository{
    Future<Either<Failure, List<Product>>> getProducts();
}