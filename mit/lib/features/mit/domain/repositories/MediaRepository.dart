import 'package:dartz/dartz.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';

abstract class MediaRepository{
    Future<Either<Failure, List<Media>>> getMedias();
}