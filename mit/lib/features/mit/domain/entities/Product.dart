import 'package:equatable/equatable.dart';

class Product extends Equatable {
    final int id;
    final String description;
    final double price;
    final bool isSale;

    const Product(this.id, this.description, this.price, this.isSale): super();

    @override
    String toString() => "{id: $id, description: $description, price: $price, isSale: $isSale}";

    @override
    List<Object> get props => [id, description, price, isSale];
}