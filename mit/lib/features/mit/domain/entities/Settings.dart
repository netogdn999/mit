import 'package:equatable/equatable.dart';

class Settings extends Equatable {
    final int id;
    final int durationMediaTransition;
    final int durationProductPageTransition;
    final int quantityProductsPerPage;
    final bool isUpdatedProducts;
    final bool isUpdatedMedias;

    const Settings(this.id, this.durationMediaTransition, this.durationProductPageTransition, this.quantityProductsPerPage, this.isUpdatedProducts, this.isUpdatedMedias): super();

    @override
    String toString() => '{id: $id, durationMediaTransition: $durationMediaTransition, durationProductPageTransition: $durationProductPageTransition, quantityProductsPerPage: $quantityProductsPerPage, isUpdatedProducts: $isUpdatedProducts, isUpdatedMedias: $isUpdatedMedias}';

    @override
    List<Object> get props => [id, durationMediaTransition, durationProductPageTransition, quantityProductsPerPage, isUpdatedProducts, isUpdatedMedias];
}