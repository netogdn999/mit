import 'package:equatable/equatable.dart';

class Media extends Equatable {
    final int id;
    final String path;

    const Media(this.id, this.path): super();

    @override
    String toString() => '{id: $id, path: $path}';

    @override
    List<Object> get props => [id, path];
}