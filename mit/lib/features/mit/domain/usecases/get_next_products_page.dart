import 'package:mit/features/mit/domain/entities/Product.dart';

class GetNextProductsPage{
    List<Product> products = List();
    int _productsPerPage = 10;
        set productsPerPage(int value) => _productsPerPage = value;
    int durationTransitionPage = 2;
    int _totalPage = 0;
    int _currentPage = 1;

    GetNextProductsPage();

    List<Product> call() {
        calculateTotalPage();
        List<Product> result;
        if(_productsPerPage > products.length) return products;
        final startId = (_currentPage-1)*_productsPerPage;
        final endId = _currentPage*_productsPerPage;
        if(_currentPage < _totalPage){
            result = _currentPage == 0
                ? products.sublist(0, endId)
                : products.sublist(startId, endId);
            _currentPage++;
        } else {
            result = products.sublist(startId);
            _currentPage = 0;
        }
        return result;
    }

    void calculateTotalPage(){
        if(products.isNotEmpty) {
            final int size = products.length;
            _totalPage = (size % _productsPerPage > 0 ? size/_productsPerPage + 1 : size / _productsPerPage).toInt();
        }
    }
}