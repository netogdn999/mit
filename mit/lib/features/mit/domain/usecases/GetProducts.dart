import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';
import 'package:mit/features/mit/domain/repositories/ProductRepository.dart';

class GetProducts extends Equatable {
    final ProductRepository repository;

    const GetProducts(this.repository): super();

    Future<Either<Failure, List<Product>>> call() async => repository.getProducts();

    @override
    List<Object> get props => [repository];
}