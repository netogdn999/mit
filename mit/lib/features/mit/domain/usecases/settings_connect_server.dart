import 'dart:typed_data';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/features/mit/domain/connection/settings_socket_communication.dart';

class SettingsConnectServer extends Equatable{
    final SettingsSocketCommunication socketCommunication;

    const SettingsConnectServer(this.socketCommunication);

    Future<Either<SocketFailure, bool>> connect(String host, port)
        async => host.isNotEmpty
            ? socketCommunication.connect(host, port)
            : Left(SocketFailure());

    void getSettings(Function(Uint8List data) success, Function(Object error) failure, Function() disconnect) => socketCommunication.getSettings(success, failure, disconnect);

    @override
    List<Object> get props => [socketCommunication];

}