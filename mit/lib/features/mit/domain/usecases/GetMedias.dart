import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';
import 'package:mit/features/mit/domain/repositories/MediaRepository.dart';

class GetMedias extends Equatable {
    final MediaRepository repository;

    const GetMedias(this.repository): super();

    Future<Either<Failure, List<Media>>> call() async => repository.getMedias();

    @override
    List<Object> get props => [repository];
}