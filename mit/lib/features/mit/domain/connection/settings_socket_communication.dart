import 'dart:async';
import 'dart:typed_data';

import 'package:dartz/dartz.dart';
import 'package:mit/core/error/failures.dart';

abstract class SettingsSocketCommunication {
    /// Throws a [SocketException] for all error codes
    Future<Either<SocketFailure, bool>> connect(String host, int port);

    void getSettings(Function(Uint8List data) success, Function(Object error) failure, Function() disconnect);
}