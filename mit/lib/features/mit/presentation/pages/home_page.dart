import 'package:flutter/material.dart';
import 'package:mit/features/mit/presentation/pages/providers/media_provider.dart';
import 'package:mit/features/mit/presentation/pages/providers/product_provider.dart';
import 'package:mit/features/mit/presentation/pages/providers/settings_provider.dart';

class HomePage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.all(5.0),
            child: Stack(children: <Widget>[
                Row(children: <Widget>[
                    Container(
                        margin: const EdgeInsets.only(right: 5.0),
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: mediasProvider()
                    ),
                    Expanded(
                        child: Container(
                            margin: const EdgeInsets.only(left: 5.0),
                            child: productProvider()
                        ),
                    )
                ]),
                Container(
                    alignment: Alignment.topRight,
                    child: settingsProvider()
                )
            ])
        );
    }
}