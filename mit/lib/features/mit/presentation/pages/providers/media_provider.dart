import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mit/features/mit/presentation/bloc/media_bloc.dart';
import 'package:mit/features/mit/presentation/widgets/error_display.dart';
import 'package:mit/features/mit/presentation/widgets/loading_widget.dart';
import 'package:mit/features/mit/presentation/widgets/media_display.dart';
import 'package:mit/features/mit/presentation/widgets/media_state_initialization.dart';

import '../../../injection_container.dart';

BlocProvider<MediaBloc> mediasProvider() {
    return BlocProvider(
        create: (_) => sl<MediaBloc>(),
        child: BlocBuilder<MediaBloc, MediaState>(
            builder: (context, state){
                if(state is MediaInitial)
                    return MediaStateInitialization();
                if(state is MediaLoading)
                    return LoadingWidget();
                if(state is MediaLoaded)
                    return MediaDisplay(medias: state.medias);
                return ErrorDisplay(message: state is MediaError ? state.error : "Undefined state $state");
            }
        )
    );
}