import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mit/features/mit/presentation/bloc/settings_bloc.dart';
import 'package:mit/features/mit/presentation/widgets/settings_status.dart';

import '../../../injection_container.dart';

BlocProvider<SettingsBloc> settingsProvider() {
    return BlocProvider(
        create: (_) => sl<SettingsBloc>(),
        child: BlocBuilder<SettingsBloc, SettingsState>(
            builder: (context, state){
                if(state is SettingsInitial) {
                    addConnectServer(context);
                    return SettingsStatus(color: Colors.black);
                }if(state is SettingsEstablishingCommunicationToServer)
                    return SettingsStatus(color: Colors.yellow);
                if(state is SettingsEstablishedCommunicationSuccess)
                    return SettingsStatus(color: Colors.green);
                if(state is SettingsUpdateProducts)
                    return SettingsStatus(color: Colors.red);
                if(state is SettingsUpdateMedias)
                    return SettingsStatus(color: Colors.grey);
                return Text(state is SettingsEstablishedCommunicationError ? state.error : "Undefined state $state");
            }
        )
    );
}

void addConnectServer(BuildContext context) {
    BlocProvider.of<SettingsBloc>(context)
        .add(ConnectToServer("192.168.0.9", 4747));
}