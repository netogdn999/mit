import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mit/features/mit/presentation/bloc/product_bloc.dart';
import 'package:mit/features/mit/presentation/widgets/error_display.dart';
import 'package:mit/features/mit/presentation/widgets/loading_widget.dart';
import 'file:///D:/Include/projetos/mobile/mit/mit/lib/features/mit/presentation/widgets/productsList/product_list_view.dart';
import 'package:mit/features/mit/presentation/widgets/product_state_initialization.dart';

import '../../../injection_container.dart';

BlocProvider<ProductBloc> productProvider() {
    return BlocProvider(
        create: (_) => sl<ProductBloc>(),
        child: BlocBuilder<ProductBloc, ProductState>(
            builder: (context, state) {
                if(state is ProductInitial)
                    return ProductStateInitialization();
                if(state is ProductLoading)
                    return LoadingWidget();
                if(state is ProductLoaded)
                    return ProductListView(products: state.products);
                return ErrorDisplay(message: state is ProductError ? state.error : "Undefined state");
            }
        )
    );
}