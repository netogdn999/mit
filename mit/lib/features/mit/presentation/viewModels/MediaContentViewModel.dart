import 'package:flutter/widgets.dart';
import 'package:mit/features/mit/data/MediaRepository.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';

import 'Repository.dart';

class MediaContentViewModel extends ChangeNotifier{
    List<Media> medias;
    Media currentMedia;
    int currentMediaId;
    Repository repository = MediaRepository();

    void loadMedias(){
        if(medias.isEmpty)
            medias = repository.fetchData();
    }

    void getNextMedia(){
        if(currentMediaId >= medias.length)
            currentMediaId = 0;
        currentMedia = medias[currentMediaId];
        currentMediaId ++;
        notifyListeners();
    }
}