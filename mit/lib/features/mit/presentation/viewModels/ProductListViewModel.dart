import 'package:flutter/widgets.dart';
import 'package:mit/features/mit/data/ProductRepository.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';

import 'Repository.dart';

class ProductListViewModel extends ChangeNotifier{
    List<Product> productsCurrentPage;
    List<Product> allProducts;
    int currentPage = 0;
    int lastProductIdCurrentPage = 0;
    Repository repository = ProductRepository();

    void loadProducts(){
        if(allProducts == null || allProducts.isEmpty)
            allProducts = repository.fetchData();
    }

    void getNextRangeProducts(int range){
        currentPage ++;
        var endProductIdNextPage = currentPage * range;
        if(productsCurrentPage == null || productsCurrentPage.isEmpty || lastProductIdCurrentPage == allProducts.length){
            productsCurrentPage = allProducts.getRange(0, range).toList();
            currentPage = 1;
            lastProductIdCurrentPage = range;
        }else if(endProductIdNextPage >= allProducts.length && lastProductIdCurrentPage <= allProducts.length){
            productsCurrentPage = allProducts.getRange(lastProductIdCurrentPage, allProducts.length).toList();
            currentPage ++;
            lastProductIdCurrentPage = allProducts.length;
        }else{
            productsCurrentPage = allProducts.getRange(lastProductIdCurrentPage, endProductIdNextPage).toList();
            currentPage ++;
            lastProductIdCurrentPage = endProductIdNextPage;
        }
        notifyListeners();
    }
}