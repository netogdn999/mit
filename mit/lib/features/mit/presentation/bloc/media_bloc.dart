import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';
import 'package:mit/features/mit/domain/usecases/GetMedias.dart';

part 'media_event.dart';
part 'media_state.dart';

class MediaBloc extends Bloc<MediaEvent, MediaState> {
    final GetMedias getMedias;

    MediaBloc({this.getMedias});

    @override
    MediaState get initialState => MediaInitial();

    @override
    Stream<MediaState> mapEventToState(MediaEvent event) async* {
        yield MediaLoading();
        final failureOrMedias = await getMedias();
        yield failureOrMedias.fold(
                (failure) => MediaError(failure.message),
                (medias) => MediaLoaded(medias));
    }


}
