part of 'settings_bloc.dart';

abstract class SettingsEvent extends Equatable {
    const SettingsEvent();
}

class ConnectToServer extends SettingsEvent {
    final String host;
    final int port;

    const ConnectToServer(this.host, this.port): super();

    @override
    List<Object> get props => [host, port];
}

class UpdatedMedias extends SettingsEvent {
    final int durationMediaTransition;

    const UpdatedMedias(this.durationMediaTransition): super();

    @override
    List<Object> get props => [durationMediaTransition];
}

class UpdatedProducts extends SettingsEvent {
    final int durationProductPageTransition;
    final int quantityProductsPerPage;

    const UpdatedProducts(this.durationProductPageTransition, this.quantityProductsPerPage): super();

    @override
    List<Object> get props => [durationProductPageTransition, quantityProductsPerPage];
}

class ConnectionError extends SettingsEvent {
    final String message;

    const ConnectionError(this.message):super();

    @override
    List<Object> get props => [message];
}