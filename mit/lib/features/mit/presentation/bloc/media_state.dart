part of 'media_bloc.dart';

abstract class MediaState extends Equatable {
    const MediaState(): super();
}

class MediaInitial extends MediaState {
    @override
    List<Object> get props => [];
}

class MediaLoading extends MediaState {
    @override
    List<Object> get props => [];
}

class MediaLoaded extends MediaState {
    final List<Media> medias;

    const MediaLoaded(this.medias): super();

    @override
    List<Object> get props => [medias];
}

class MediaError extends MediaState {
    final String error;

    const MediaError(this.error): super();

    @override
    List<Object> get props => [error];
}