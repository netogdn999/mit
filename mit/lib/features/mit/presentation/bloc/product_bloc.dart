import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';
import 'package:mit/features/mit/domain/usecases/GetProducts.dart';
import 'package:mit/features/mit/domain/usecases/get_next_products_page.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
    final GetProducts getProducts;
    final GetNextProductsPage nextProductsPage;

    ProductBloc(this.getProducts, this.nextProductsPage)
        : assert(getProducts != null),
          assert(nextProductsPage != null);

    @override
    ProductState get initialState => ProductInitial();

    @override
    Stream<ProductState> mapEventToState(ProductEvent event) async* {
        if(event is GetProductsEvent)
            yield* _loadProducts();
        else if(event is UpdateProductsValuesEvent){
            nextProductsPage.productsPerPage = event.productsPerPage;
            nextProductsPage.durationTransitionPage = event.durationTransitionPage;
            yield* _loadProducts();
        } else if(event is GetNextProductsPageEvent) {
            final result = nextProductsPage();
            yield ProductLoopPage(result);
            _transitionPageLoop();
        }
    }

    Stream<ProductState> _loadProducts() async* {
        yield ProductLoading();
        final failureOrProducts = await getProducts();
        yield failureOrProducts.fold(
                (failure) => ProductError(failure.message),
                (products) {
                final result = nextProductsPage();
                return ProductLoaded(result);
            }
        );
    }

    void _transitionPageLoop() =>
        Future.delayed(Duration(seconds: nextProductsPage.durationTransitionPage), () => add(GetNextProductsPageEvent()));
}
