part of 'settings_bloc.dart';

abstract class SettingsState extends Equatable {
    const SettingsState();
}

class SettingsInitial extends SettingsState {
    @override
    List<Object> get props => [];
}

class SettingsEstablishingCommunicationToServer extends SettingsState {
    @override
    List<Object> get props => [];
}

class SettingsEstablishedCommunicationError extends SettingsState {
    final String error;

    const SettingsEstablishedCommunicationError(this.error): super();

    @override
    List<Object> get props => [error];
}

class SettingsEstablishedCommunicationSuccess extends SettingsState {
    @override
    List<Object> get props => [];
}

class SettingsUpdateMedias extends SettingsState {
    final int durationMediaTransition;

    const SettingsUpdateMedias(this.durationMediaTransition): super();

    @override
    List<Object> get props => [durationMediaTransition];
}

class SettingsUpdateProducts extends SettingsState {
    final int durationProductPageTransition;
    final int quantityProductsPerPage;

    const SettingsUpdateProducts(this.durationProductPageTransition, this.quantityProductsPerPage):super();

    @override
    List<Object> get props => [durationProductPageTransition, quantityProductsPerPage];
}
