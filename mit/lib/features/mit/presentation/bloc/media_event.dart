part of 'media_bloc.dart';

abstract class MediaEvent extends Equatable {
    const MediaEvent();
}

class GetNextMediasPage extends MediaEvent {
    @override
    List<Object> get props => [];

}

class UpdateMediasValues extends MediaEvent {
    @override
    List<Object> get props => [];

}