part of 'product_bloc.dart';

abstract class ProductEvent extends Equatable {
    const ProductEvent();
}

class GetProductsEvent extends ProductEvent {
    @override
    List<Object> get props => [];
}

class GetNextProductsPageEvent extends ProductEvent {
    @override
    List<Object> get props => [];
}

class UpdateProductsValuesEvent extends ProductEvent {
    final int productsPerPage;
    final int durationTransitionPage;

    const UpdateProductsValuesEvent(this.productsPerPage, this.durationTransitionPage): super();

    @override
    List<Object> get props => [productsPerPage];
}
