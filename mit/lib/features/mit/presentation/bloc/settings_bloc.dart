import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/core/util/input_converter.dart';
import 'package:mit/features/mit/data/models/SettingsModel.dart';
import 'package:mit/features/mit/domain/usecases/settings_connect_server.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
    final SettingsConnectServer connectServer;
    final InputConverter converter;

    SettingsBloc({this.connectServer, this.converter});

    @override
    SettingsState get initialState => SettingsInitial();

    @override
    Stream<SettingsState> mapEventToState(SettingsEvent event) async* {
        if(event is ConnectToServer) {
            yield SettingsEstablishingCommunicationToServer();
            final failureOrSocket = await connectServer.connect(event.host, event.port);
            yield failureOrSocket.fold(
                    (failure) => SettingsEstablishedCommunicationError(SOCKET_FAILURE_MESSAGE),
                    (_) {
                        connectServer.getSettings(socketSuccessMessage, socketFailureMessage, socketDisconnect);
                        return SettingsEstablishedCommunicationSuccess();
                    }
            );
        }if(event is UpdatedMedias)
            yield SettingsUpdateMedias(event.durationMediaTransition);
        if(event is UpdatedProducts)
            yield SettingsUpdateProducts(event.durationProductPageTransition, event.quantityProductsPerPage);
        if(event is ConnectionError)
            yield SettingsEstablishedCommunicationError(event.message);
    }

    socketSuccessMessage(Uint8List data) {
        final jsonString = converter.uint8ListToString(data);
        final result = SettingsModel.fromJson(json.decode(jsonString));
        if(result.isUpdatedMedias)
            add(UpdatedMedias(result.durationMediaTransition));
        if(result.isUpdatedProducts)
            add(UpdatedProducts(result.durationProductPageTransition, result.quantityProductsPerPage));
    }

    socketFailureMessage(Object error) {
        add(ConnectionError(SOCKET_FAILURE_MESSAGE));
    }

    socketDisconnect() {
        add(ConnectionError(SOCKET_FAILURE_MESSAGE));
    }
}
