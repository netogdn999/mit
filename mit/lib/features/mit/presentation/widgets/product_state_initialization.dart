import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mit/features/mit/presentation/bloc/product_bloc.dart';

class ProductStateInitialization extends StatelessWidget{
    @override
    Widget build(BuildContext context) {
        addGetProducts(context);
        return Center(
            child: CircularProgressIndicator(),
        );
    }

    void addGetProducts(BuildContext context) {
        BlocProvider.of<ProductBloc>(context)
            .add(GetProductsEvent());
    }
}