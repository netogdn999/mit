import 'package:flutter/material.dart';
import 'package:mit/core/util/OutputConverter.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';
import 'file:///D:/Include/projetos/mobile/mit/mit/lib/features/mit/presentation/widgets/productsList/item_list_view_products_background.dart';

class ProductDisplay extends StatelessWidget {
    final OutputConverter converter = const OutputConverter();
    final Product product;

    const ProductDisplay({Key key, @required this.product}): super(key: key);

    @override
    Widget build(BuildContext context) {
        return Stack(children: <Widget> [
            CustomPaint(
                size: Size.infinite,
                painter: ItemListViewProdutcsBackground()
            ),
            Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 8.0),
                    width: 105.0,
                    alignment: Alignment.centerLeft,
                    child: Text(
                        '${product.id}',
                        style: TextStyle(color: Colors.white),
                    )
                ),
                Expanded(
                    child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                            product.description,
                            style: TextStyle(color: Colors.white)
                        ),
                    ),
                ),
                Container(
                    margin: EdgeInsets.only(right: 8.0),
                    alignment: Alignment.centerRight,
                    width: 105.0,
                    child: Text(
                        converter.numberToMoneyPerKilo(product.price),
                        style: TextStyle(color: Colors.white),
                    )
                )]
            )]
        );
    }
}