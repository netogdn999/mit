import 'package:flutter/material.dart';

class ItemListViewProdutcsBackground extends CustomPainter{
    final Color startColor;
    final Color endColor;

    const ItemListViewProdutcsBackground({this.startColor = Colors.red, this.endColor = Colors.black}):super();

    @override
    void paint(Canvas canvas, Size size) {
        final paint = Paint()
            ..style = PaintingStyle.fill
            ..shader = LinearGradient(
                colors: [startColor, endColor],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter
            ).createShader(Rect.fromPoints(
                Offset(0.0, 0.0),
                Offset(size.width.toDouble(), size.height.toDouble())
            ));
        canvas.drawPath(_createMidlePath(size), paint);
        canvas.drawShadow(_createLeftPathShadow(size), Colors.black, 2.0, true);
        canvas.drawShadow(_createRightPathShadow(size), Colors.black, 2.0, true);
        canvas.drawPath(_createLeftPath(size), paint);
        canvas.drawPath(_createRightPath(size), paint);
    }

    Path _createMidlePath(Size size){
        final path = Path();
        final width = size.width.toDouble();
        final height = size.height.toDouble();
        path.moveTo(0.0, 3.0);
        path.lineTo(width, 3.0);
        path.lineTo(width, height - 3.0);
        path.lineTo(0.0, height - 3.0);
        path.close();
        return path;
    }

    Path _createLeftPath(Size size){
        final path = Path();
        final width = size.width.toDouble()/10;
        final height = size.height.toDouble();
        path.moveTo(0.0, 0.0);
        path.lineTo(width * 3 - 30.0, 0.0);
        path.lineTo(width * 3, height);
        path.lineTo(0.0, height);
        path.close();
        return path;
    }

    Path _createLeftPathShadow(Size size){
        final path = Path();
        final width = size.width.toDouble()/10;
        final height = size.height.toDouble();
        path.moveTo(0.0, 0.0);
        path.lineTo(width * 3 - 25.0, 0.0);
        path.lineTo(width * 3 + 5.0, height - 1.0);
        path.lineTo(0.0, height - 1.0);
        path.close();
        return path;
    }

    Path _createRightPath(Size size){
        final path = Path();
        final width = size.width.toDouble()/10;
        final height = size.height.toDouble();
        path.moveTo(width * 7 + 30.0, 0.0);
        path.lineTo(width * 7, height);
        path.lineTo(width * 10, height);
        path.lineTo(width * 10, 0.0);
        path.close();
        return path;
    }

    Path _createRightPathShadow(Size size){
        final path = Path();
        final width = size.width.toDouble()/10;
        final height = size.height.toDouble();
        path.moveTo(width * 7 + 25.0, 0.0);
        path.lineTo(width * 7 - 5.0, height - 1.0);
        path.lineTo(width * 10, height - 1.0);
        path.lineTo(width * 10, 0.0);
        path.close();
        return path;
    }

    @override
    bool shouldRepaint(CustomPainter oldDelegate) => false;
}