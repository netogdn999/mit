import 'package:flutter/material.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';
import 'product_display.dart';

class ProductListView extends StatelessWidget {
    final List<Product> products;

    const ProductListView({Key key, @required this.products}) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return ListView.builder(
            itemCount: products.length,
            itemBuilder: (BuildContext context, int index) => Container(
                margin: EdgeInsets.only(bottom: 2),
                alignment: Alignment.centerLeft,
                height: 30,
                child: ProductDisplay(product: products[index])
            )
        );
    }
}