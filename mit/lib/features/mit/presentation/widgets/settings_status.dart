import 'package:flutter/material.dart';

class SettingsStatus extends StatelessWidget{
    final Color color;

    const SettingsStatus({Key key, @required this.color}) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Container(
            color: color,
            child: SizedBox(
                width: 5,
                height: 5
            )
        );
    }
}