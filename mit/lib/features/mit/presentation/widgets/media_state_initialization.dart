import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mit/features/mit/presentation/bloc/media_bloc.dart';

class MediaStateInitialization extends StatelessWidget{
    @override
    Widget build(BuildContext context) {
        addGetMedias(context);
        return Center(
            child: CircularProgressIndicator(),
        );
    }

    void addGetMedias(BuildContext context) {
        BlocProvider.of<MediaBloc>(context)
            .add(GetNextMediasPage());
    }
}