import 'package:flutter/services.dart';

void freezOnLandscapeScreenOrientation(){
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft
    ]);
}