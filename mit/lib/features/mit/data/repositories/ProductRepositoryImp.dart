import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/core/network/NetworkInfo.dart';
import 'package:mit/features/mit/data/datasources/ProductLocalDataSource.dart';
import 'package:mit/features/mit/data/datasources/ProductRemoteDataSource.dart';
import 'package:mit/features/mit/domain/entities/Product.dart';
import 'package:mit/features/mit/domain/repositories/ProductRepository.dart';

class ProductRepositoryImp extends Equatable implements ProductRepository {
    final ProductRemoteDataSource remoteDataSource;
    final ProductLocalDataSource localDataSource;
    final NetworkInfo networkInfo;

    const ProductRepositoryImp({this.remoteDataSource, this.localDataSource, this.networkInfo}): super();

    @override
    Future<Either<Failure, List<Product>>> getProducts() async{
        if(await networkInfo.isConnect) {
            try {
                final remoteProducts = await remoteDataSource.getProducts();
                localDataSource.putProducts(remoteProducts);
                return Right(remoteProducts);
            } on ServerException {
                return Left(ServerFailure());
            }
        } else {
            try {
                final localProducts = await localDataSource.getProducts();
                return Right(localProducts);
            } on CacheException {
                return Left(CacheFailure());
            }
        }
    }

    @override
    List<Object> get props => [remoteDataSource, localDataSource, networkInfo];
}