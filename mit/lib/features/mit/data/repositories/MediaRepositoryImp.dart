import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/core/network/NetworkInfo.dart';
import 'package:mit/features/mit/data/datasources/MediaLocalDataSource.dart';
import 'package:mit/features/mit/data/datasources/MediaRemoteDataSource.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';
import 'package:mit/features/mit/domain/repositories/MediaRepository.dart';

class MediaRepositoryImp extends Equatable implements MediaRepository {
    final MediaRemoteDataSource remoteDataSource;
    final MediaLocalDataSource localDataSource;
    final NetworkInfo networkInfo;

    const MediaRepositoryImp({this.remoteDataSource, this.localDataSource, this.networkInfo}): super();

    @override
    Future<Either<Failure, List<Media>>> getMedias() async {
        if(await networkInfo.isConnect) {
            try {
                final remoteMedias = await remoteDataSource.getMedias();
                localDataSource.putMedias(remoteMedias);
                return Right(remoteMedias);
            } on ServerException {
                return Left(ServerFailure());
            }
        } else {
            try {
                final localMedias = await localDataSource.getMedias();
                return Right(localMedias);
            } on CacheException {
                return Left(CacheFailure());
            }
        }
    }

    @override
    List<Object> get props => [remoteDataSource, localDataSource, networkInfo];
}
