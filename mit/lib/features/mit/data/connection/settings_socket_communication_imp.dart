import 'dart:async';
import 'dart:typed_data';

import 'package:dartz/dartz.dart';
import 'package:mit/core/error/exceptions.dart';
import 'package:mit/core/error/failures.dart';
import 'package:mit/core/network/socket_communication.dart';
import 'package:mit/features/mit/domain/connection/settings_socket_communication.dart';

class SettingsSocketCommunicationImp implements SettingsSocketCommunication {
    final SocketCommunication socketCommunication;

    SettingsSocketCommunicationImp(this.socketCommunication);

    @override
    Future<Either<SocketFailure, bool>> connect(String host, int port) async{
        try{
            await socketCommunication.connect(host, port);
            return Right(true);
        } on SettingSocketException {
            return Left(SocketFailure());
        }
    }

    @override
    void getSettings(Function(Uint8List data) success, Function(Object error) failure, Function() disconnect) =>
        socketCommunication.getSettings(success, failure, disconnect);
}