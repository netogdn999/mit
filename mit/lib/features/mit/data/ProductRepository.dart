import 'package:mit/features/mit/domain/entities/Product.dart';
import 'package:mit/features/mit/presentation/viewModels/Repository.dart';

import 'ProductDao.dart';

class ProductRepository implements Repository {
    static final ProductRepository _instance = ProductRepository.getInstance();
    ProductDao dao;

    factory ProductRepository() => _instance;

    ProductRepository.getInstance() {
        dao = ProductDao();
    }

    List<Product> fetchData() => dao.fetchData();
}