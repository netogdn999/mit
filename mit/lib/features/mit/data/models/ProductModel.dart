import 'package:mit/features/mit/domain/entities/Product.dart';

class ProductModel extends Product {

    const ProductModel(int id, String description, double price, bool isSale): super(id, description, price, isSale);

    factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        json['id'],
        json['description'],
        json['price'],
        json['isSale']
    );

    Map<String, dynamic> toJson() => {
        'id': this.id,
        'description': this.description,
        'price': this.price,
        'isSale': this.isSale
    };
}