import 'package:mit/features/mit/domain/entities/Media.dart';

class MediaModel extends Media {

    const MediaModel(int id, String path): super(id, path);

    factory MediaModel.fromJson(Map<String, dynamic> json) => MediaModel(
        json['id'],
        json['path']
    );

    Map<String, dynamic> toJson() => {
        'id': this.id,
        'path': this.path
    };
}