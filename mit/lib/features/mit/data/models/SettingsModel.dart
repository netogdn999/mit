import 'package:mit/features/mit/domain/entities/Settings.dart';

class SettingsModel extends Settings {

    const SettingsModel(int id, int durationMediaTransition, int durationProductPageTransition, int quantityProductsPerPage, bool isUpdatedProducts, bool isUpdatedMedias): super(id, durationMediaTransition, durationProductPageTransition, quantityProductsPerPage, isUpdatedProducts, isUpdatedMedias);

    factory SettingsModel.fromJson(Map<String, dynamic> json) => SettingsModel(
        json['id'],
        json['durationMediaTransition'],
        json['durationProductPageTransition'],
        json['quantityProductsPerPage'],
        json['isUpdatedProducts'],
        json['isUpdatedMedias']
    );

    Map<String, dynamic> toJson() => {
        'id': this.id,
        'durationMediaTransition': this.durationMediaTransition,
        'durationProductPageTransition': this.durationProductPageTransition,
        'quantityProductsPerPage': this.quantityProductsPerPage,
        'isUpdatedProducts': this.isUpdatedProducts,
        'isUpdatedMedias': this.isUpdatedMedias
    };
}