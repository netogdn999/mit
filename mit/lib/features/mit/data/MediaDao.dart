import 'package:mit/features/mit/domain/entities/Media.dart';

class MediaDao {
    List<Media> fetchData() => List<Media>();
}