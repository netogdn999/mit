import 'dart:convert';

import 'package:mit/core/error/exceptions.dart';
import 'package:mit/features/mit/data/models/MediaModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class MediaLocalDataSource {
    /// Throws [NoLocalDataException] if no cached data is present
    Future<List<MediaModel>> getMedias();

    Future<void> putMedias(List<MediaModel> medias);
}

const CACHED_MEDIAS = 'CACHED_MEDIAS';

class MediaLocalDataSourceImp implements MediaLocalDataSource {
    final SharedPreferences sharedPreferences;

    MediaLocalDataSourceImp({this.sharedPreferences});

    @override
    Future<List<MediaModel>> getMedias() {
        final jsonString = sharedPreferences.getString(CACHED_MEDIAS);
        return jsonString == null
            ? throw CacheException()
            : Future.value((json.decode(jsonString) as List<dynamic>).map((json) => MediaModel.fromJson(json)).toList());
    }

    @override
    Future<void> putMedias(List<MediaModel> products) => sharedPreferences.setString(CACHED_MEDIAS, json.encode(products.map((product) => product.toJson()).toList()));
}