import 'dart:convert';

import 'package:mit/core/error/exceptions.dart';
import 'package:mit/features/mit/data/models/ProductModel.dart';
import 'package:http/http.dart' as http;

abstract class ProductRemoteDataSource {
    /// Throws a [ServerException] for all error codes
    Future<List<ProductModel>> getProducts();
}

class ProductRemoteDataSourceImp implements ProductRemoteDataSource {
    final http.Client client;
    // Todo: Apagar para a produção
    final jsonStringMock = "[{\"id\":100000000,\"description\":\"Description 0 desc 0\",\"price\":10.00,\"isSale\":true},{\"id\":100000001,\"description\":\"Description 1 desc 1\",\"price\":11.00,\"isSale\":false},{\"id\":100000002,\"description\":\"Description 2 desc 2\",\"price\":12.00,\"isSale\":true},{\"id\":100000003,\"description\":\"Description 3 desc 3\",\"price\":13.00,\"isSale\":false},{\"id\":100000004,\"description\":\"Description 4 desc 4\",\"price\":14.00,\"isSale\":true},{\"id\":100000005,\"description\":\"Description 5 desc 5\",\"price\":15.00,\"isSale\":false},{\"id\":100000006,\"description\":\"Description 6 desc 6\",\"price\":16.00,\"isSale\":true},{\"id\":100000007,\"description\":\"Description 7 desc 7\",\"price\":17.00,\"isSale\":false},{\"id\":100000008,\"description\":\"Description 8 desc 8\",\"price\":18.00,\"isSale\":true},{\"id\":100000009,\"description\":\"Description 9 desc 9\",\"price\":19.00,\"isSale\":false}]";

    ProductRemoteDataSourceImp({this.client});

    @override
    Future<List<ProductModel>> getProducts() async{
//        final response = await client.get(
//            'api url',
//            headers: {'Content-Type': 'application/json'},
//        );
//        return response.statusCode == 200
//            ? (json.decode(response.body) as List<dynamic>).map((json) => ProductModel.fromJson(json)).toList()
//            : throw ServerException();
        final result = (json.decode(jsonStringMock) as List<dynamic>).map((json) => ProductModel.fromJson(json)).toList();
        return result;
    }
}