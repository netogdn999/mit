import 'dart:convert';

import 'package:mit/core/error/exceptions.dart';
import 'package:mit/features/mit/data/models/MediaModel.dart';
import 'package:http/http.dart' as http;

abstract class MediaRemoteDataSource {
    /// Throws a [ServerException] for all error codes
    Future<List<MediaModel>> getMedias();
}

class MediaRemoteDataSourceImp implements MediaRemoteDataSource {
    final http.Client client;

    //Todo: remover para produção
    final jsonStringMock = "[{\"id\": 0, \"path\": \"https://www.fwhealth.org/wp-content/uploads/2017/03/placeholder-500x500.jpg\"},{\"id\": 1,\"path\": \"https://www.fwhealth.org/wp-content/uploads/2017/03/placeholder-500x500.jpg\"}]";

    MediaRemoteDataSourceImp({this.client});

    @override
    Future<List<MediaModel>> getMedias() async{
//        final response = await client.get(
//            'api url',
//            headers: {'Content-Type': 'application/json'},
//        );
//        return response.statusCode == 200
//            ? (json.decode(response.body) as List<dynamic>).map((json) => MediaModel.fromJson(json)).toList()
//            : throw ServerException();
    return (json.decode(jsonStringMock) as List<dynamic>).map((json) => MediaModel.fromJson(json)).toList();
    }
}