import 'dart:convert';

import 'package:mit/core/error/exceptions.dart';
import 'package:mit/features/mit/data/models/ProductModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class ProductLocalDataSource {
    /// Throws [NoLocalDataException] if no cached data is present
    Future<List<ProductModel>> getProducts();

    Future<void> putProducts(List<ProductModel> products);
}

const CACHED_PRODUCTS = 'CACHED_PRODUCTS';

class ProductLocalDataSourceImp implements ProductLocalDataSource {
    final SharedPreferences sharedPreferences;

    ProductLocalDataSourceImp({this.sharedPreferences});

    @override
    Future<List<ProductModel>> getProducts() {
        final jsonString = sharedPreferences.getString(CACHED_PRODUCTS);
        return jsonString == null
            ? throw CacheException()
            : Future.value((json.decode(jsonString) as List<dynamic>).map((json) => ProductModel.fromJson(json)).toList());
    }

    @override
    Future<void> putProducts(List<ProductModel> products) => sharedPreferences.setString(CACHED_PRODUCTS, json.encode(products.map((product) => product.toJson()).toList()));
}