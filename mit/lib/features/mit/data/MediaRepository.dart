import 'package:mit/features/mit/data/MediaDao.dart';
import 'package:mit/features/mit/domain/entities/Media.dart';
import 'package:mit/features/mit/presentation/viewModels/Repository.dart';

class MediaRepository implements Repository {
    static final MediaRepository _instance = MediaRepository.getInstance();
    MediaDao dao;

    factory MediaRepository() => _instance;

    MediaRepository.getInstance() {
        dao = MediaDao();
    }

    List<Media> fetchData() => dao.fetchData();
}