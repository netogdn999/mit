import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:mit/core/network/NetworkInfo.dart';
import 'package:get_it/get_it.dart';
import 'package:mit/core/network/socket_communication.dart';
import 'package:mit/core/util/input_converter.dart';
import 'injection_containers/injection_container_media.dart' as containerMedia;
import 'injection_containers/injection_container_product.dart' as containerProduct;
import 'injection_containers/injection_container_settings.dart' as containerSettings;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

final sl = GetIt.instance;

Future<void> init() async{
    /**
     * ! Feature
     * ? Media, Product and Settings*/
    containerMedia.init();
    containerProduct.init();
    containerSettings.init();

    /**
     * ! Core */
    sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImp(sl()));
    sl.registerLazySingleton<SocketCommunication>(() => SocketCommunicationImp());
    sl.registerLazySingleton(() => InputConverter());

    /**
     * ! External */
    final sharedPreferences = await SharedPreferences.getInstance();
    sl.registerLazySingleton(() => sharedPreferences);
    sl.registerLazySingleton(() => http.Client());
    sl.registerLazySingleton(() => DataConnectionChecker());
}