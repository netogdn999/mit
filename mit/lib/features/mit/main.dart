import 'package:flutter/material.dart';
import 'presentation/pages/home_page.dart';
import 'screen_config.dart';
import 'injection_container.dart';

void main() async{
    WidgetsFlutterBinding.ensureInitialized();
    await init();
    runApp(MyApp());
}

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        freezOnLandscapeScreenOrientation();
        return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'MIT',
            theme: ThemeData(primarySwatch: Colors.blue),
            home: Scaffold(
                appBar: AppBar(title: Text('MIT')),
                body: HomePage(),
            ),
        );
    }
}