import 'package:mit/features/mit/data/connection/settings_socket_communication_imp.dart';
import 'package:mit/features/mit/domain/connection/settings_socket_communication.dart';
import 'package:mit/features/mit/domain/usecases/settings_connect_server.dart';
import 'package:mit/features/mit/presentation/bloc/settings_bloc.dart';

import '../injection_container.dart';

void init(){
    sl.registerFactory(() => SettingsBloc(connectServer: sl(), converter: sl()));

    // Use cases
    sl.registerLazySingleton(() => SettingsConnectServer(sl()));

    // Connection
    sl.registerLazySingleton<SettingsSocketCommunication>(() => SettingsSocketCommunicationImp(sl()));
}