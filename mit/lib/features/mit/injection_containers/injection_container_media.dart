import 'package:mit/features/mit/domain/repositories/MediaRepository.dart';
import 'package:mit/features/mit/data/datasources/MediaLocalDataSource.dart';
import 'package:mit/features/mit/data/datasources/MediaRemoteDataSource.dart';
import 'package:mit/features/mit/data/repositories/MediaRepositoryImp.dart';
import 'package:mit/features/mit/domain/usecases/GetMedias.dart';
import 'package:mit/features/mit/presentation/bloc/media_bloc.dart';

import '../injection_container.dart';

void init(){
    sl.registerFactory(() => MediaBloc(getMedias: sl()));

    // Use cases
    sl.registerLazySingleton(() => GetMedias(sl()));

    // Repository
    sl.registerLazySingleton<MediaRepository>(
        () => MediaRepositoryImp(
            remoteDataSource: sl(),
            localDataSource: sl(),
            networkInfo: sl()
        )
    );

    // Data sources
    sl.registerLazySingleton<MediaRemoteDataSource>(
        () => MediaRemoteDataSourceImp(
            client: sl()
        )
    );

    sl.registerLazySingleton<MediaLocalDataSource>(
        () => MediaLocalDataSourceImp(
            sharedPreferences: sl()
        )
    );
}