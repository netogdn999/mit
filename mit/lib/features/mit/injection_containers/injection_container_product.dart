import 'package:mit/features/mit/domain/repositories/ProductRepository.dart';
import 'package:mit/features/mit/data/datasources/ProductLocalDataSource.dart';
import 'package:mit/features/mit/data/datasources/ProductRemoteDataSource.dart';
import 'package:mit/features/mit/data/repositories/ProductRepositoryImp.dart';
import 'package:mit/features/mit/domain/usecases/GetProducts.dart';
import 'package:mit/features/mit/domain/usecases/get_next_products_page.dart';
import 'package:mit/features/mit/presentation/bloc/product_bloc.dart';


import '../injection_container.dart';

void init() {
    sl.registerFactory(() => ProductBloc(sl(), sl()));

    // Use cases
    sl.registerLazySingleton(() => GetProducts(sl()));
    sl.registerLazySingleton(() => GetNextProductsPage());

    // Repository
    sl.registerLazySingleton<ProductRepository>(
        () => ProductRepositoryImp(
            remoteDataSource: sl(),
            localDataSource: sl(),
            networkInfo: sl()
        )
    );

    // Data sources
    sl.registerLazySingleton<ProductRemoteDataSource>(
        () => ProductRemoteDataSourceImp(
            client: sl()
        )
    );

    sl.registerLazySingleton<ProductLocalDataSource>(
        () => ProductLocalDataSourceImp(
            sharedPreferences: sl()
        )
    );
}