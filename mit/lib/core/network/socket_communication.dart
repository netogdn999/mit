import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

abstract class SocketCommunication {
    /// Throws a [SocketException] for all error codes
    Future<void> connect(String host, int port);

    void getSettings(Function success(Uint8List data), Function failure(Object error), Function disconnect());
}

class SocketCommunicationImp implements SocketCommunication{
    Socket _socket;

    SocketCommunicationImp();

    /// Throws a [SocketException] for all error codes
    Future<void> connect(String host, int port) async =>
        _socket = await Socket.connect(host, port);

    void getSettings(Function(Uint8List data) success, Function(Object error) failure, Function() disconnect) =>
        _socket.listen((data) => success(data), onError: (error) => failure(error), onDone: () => disconnect(), cancelOnError: false);
}