import 'dart:typed_data';

class InputConverter {

    const InputConverter();

    String uint8ListToString(Uint8List data) => String.fromCharCodes(data).trim();
}