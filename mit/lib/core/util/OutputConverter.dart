import 'package:intl/intl.dart';

class OutputConverter {

    const OutputConverter();

    String numberToMoneyPerKilo(double value){
        final format = NumberFormat("#,##0.00", "pt_BR");
        return "R\$ ${format.format(value)}/Kg";
    }
}