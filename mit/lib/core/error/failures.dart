import 'package:equatable/equatable.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String SOCKET_FAILURE_MESSAGE = 'Socket Failure';

abstract class Failure extends Equatable {
    String get message => "Unexpected Error";

    const Failure(): super();
}

class ServerFailure extends Failure {
    @override
    String get message => SERVER_FAILURE_MESSAGE;

    @override
    List<Object> get props => [];
}

class CacheFailure extends Failure {
    @override
    String get message => CACHE_FAILURE_MESSAGE;

    @override
    List<Object> get props => [];
}

class SocketFailure extends Failure {
    @override
    String get message => SOCKET_FAILURE_MESSAGE;

    @override
    List<Object> get props => [];
}